'use strict'

//Cargar a express
var express = require("express");

//Cargar Body-Parser
var bodyParser = require("body-parser");

//Cargar CORS para permisos api
var cors = require('cors');

//Instancia de express
var app = express();

//Reciba datos hasta 50mb
app.use(bodyParser.urlencoded({
    limit: "50mb",
    extended: true
}));

//Configuramos los json
app.use(bodyParser.json({
    limit: "50mb"
}));

//Middleware para los CORS (sControl de Acceso HTTP)
app.use(cors());

//Cargar las routes de la aplicación
var routeAmenitie = require("./routes/routeAmenitie");
app.use("/api", routeAmenitie);

var routeHotel = require("./routes/routeHotel");
app.use("/api", routeHotel);

module.exports = app;