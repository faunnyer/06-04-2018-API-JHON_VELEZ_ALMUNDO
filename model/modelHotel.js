'use strict'

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var modelHotel = Schema({

    name: String,
    stars: Number,
    price: Number,
    image: String,
    
    amenities: [
        { type : Schema.ObjectId, ref: 'Amenitie' }
    ]

});

module.exports = mongoose.model('Hotel', modelHotel);