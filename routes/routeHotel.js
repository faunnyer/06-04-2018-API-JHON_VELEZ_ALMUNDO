'use strict'

var express = require('express');
var controllerHotel = require("./../controllers/controllerHotel");

var api = express.Router();

//CONSULTAMOS HOTEL POR ID DE HOTEL
api.get("/hotel/:idHotel", controllerHotel.getHotel);

//CONSULTAMOS TOOS LOS HOTELES
api.get("/hotels", controllerHotel.listHotels);

//GUARDAMOS EL HOTEL
api.post("/hotel", controllerHotel.saveHotel);

//MODIFICAMOS EL HOTEL
api.patch("/hotel/:idHotel", controllerHotel.updateHotel);

//ELIMINAMOS EL HOTEL
api.delete("/hotel/:idHotel", controllerHotel.deleteHotel);

//CONSULTAMOS LA IMAGEN DEL HOTEL
api.get("/hotel/image/:image", controllerHotel.recoverImage);

module.exports = api;