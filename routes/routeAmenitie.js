'use strict'

var express = require('express');
var controllerAmenitie = require("./../controllers/controllerAmenitie");

var api = express.Router();

//CONSULTAMOS AMENITIE POR ID DE AMENITIE
api.get("/amenitie/:idAmenitie", controllerAmenitie.getAmenitie);

//CONSULTAMOS TODOS LOS AMENITIE
api.get("/amenities", controllerAmenitie.listAmenities);

//GUARDAMOS AMENITIE
api.post("/amenitie", controllerAmenitie.saveAmenitie);

//MODIFICAMOS AMENITIE
api.patch("/amenitie/:idAmenitie", controllerAmenitie.updateAmenitie);

//ELIINAMOS
api.delete("/amenitie/:idAmenitie", controllerAmenitie.deleteAmenitie);

//CONSULTAMOS LA IMAGEN DEL AMENITIE
api.get("/amenitie/image/:image", controllerAmenitie.recoverImage);

module.exports = api;