'use strict'

var modelHotel = require("./../model/modelHotel");
var modelAmenitie = require("./../model/modelAmenitie");
var miConstData = require("./../data/data");
var fs = require('fs');
var path = require('path');

var multer = require('multer')


//CREAMOS UN DIRECTORIO APRA GUARDAR LAS IMAGENES DE LOS HOTELES

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/hotels')
    },
    filename: function (req, file, cb) {
        var imageName = file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];
        cb(null, imageName)
    }
});


// validamos la extension de la imagen del hotel que queremos subir
var upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) { //Archivo a procesar
        let extension = (file.originalname.split('.')[file.originalname.split('.').length - 1]).toLowerCase();
        if (['png', 'jpg', 'gif', 'jpeg' , 'svg'].indexOf(extension) === -1) {
            return callback(new Error('La extensión no cumple'));
        }
        callback(null, true);
    }
}).single('file');




//VALIDAMOS EL HOTEL Y SUS CAMPOS REQUERIDOS

function validateHotel(req) {

    let valid = true;
    let message = '';

    if (!req.file) {
        valid = false;
        message += 'La imagen del hotel es requerida. ';
    }

    if (req.body.name !== undefined) {

        if (req.body.name.trim() === '') {
            valid = false;
            message += 'El nombre del hotel es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de nombre del hotel es requerido. ';
    }

    if (req.body.stars !== undefined) {

        if (req.body.stars.trim() === '') {
            valid = false;
            message += 'La cantidad de estrellas para el hotel es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de estrellas de hotel es requerido. ';
    }

    if (req.body.price !== undefined) {

        if (req.body.price.trim() === '') {
            valid = false;
            message += 'El precio del hotel es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de precio del hotel es requerido. ';
    }
    

    return {
        valid: valid,
        message: message
    };
}

//VALIDAMOS LA MODIFICACION DEL HOTEL

function validateHotelUpdate(req) {

    let valid = true;
    let message = '';

    if (req.body.name !== undefined) {

        if (req.body.name.trim() === '') {
            valid = false;
            message += 'El nombre del hotel es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de nombre del hotel es requerido. ';
    }

    if (req.body.stars !== undefined) {

        if (req.body.stars.trim() === '') {
            valid = false;
            message += 'La cantidad de estrellas para el hotel es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de estrellas de hotel es requerido. ';
    }

    if (req.body.price !== undefined) {

        if (req.body.price.trim() === '') {
            valid = false;
            message += 'El precio del hotel es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de precio del hotel es requerido. ';
    }

    return {
        valid: valid,
        message: message
    };
}

// GUARDAMOS EL HOTEL

function saveHotel(req, res) {

    var hotel = new modelHotel();

    upload(req, res, function (err) {

        if (err) {
            res.status(500).send({
                message: "La extensión no es correcta",
                object: null,
                response: false
            });

            return;
        }

        let isValid = validateHotel(req);

        if (isValid.valid == false) {

            res.status(422).send({
                message: isValid.message,
                object: null,
                response: false
            });

            return;

        } else {

            hotel.name = req.body.name;
            hotel.stars = req.body.stars;
            hotel.price = req.body.price;
            hotel.image = req.file.filename;

            var amenities = req.body.amenities.split(',');

            amenities.forEach(idAmenitie => {
                var amenitie = new modelAmenitie();
                amenitie._id = idAmenitie.trim();
                
                hotel.amenities.push(amenitie._id);

            });

            hotel.save((error, newHotel) => {

                if (error) {

                    res.status(500).send({
                        message: "Ocurrio un error en el servidor al intentar guardar.",
                        object: null,
                        response: false
                    });

                } else {

                    if (!newHotel) {

                        res.status(400).send({
                            message: "Ocurrio un error al intentar guardar",
                            object: null,
                            response: false
                        });

                    } else {

                        res.status(200).send({
                            message: "Hotel guardado correctamente",
                            object: newHotel,
                            response: true
                        });

                    }

                }

            });

        }
    });
}


//MODIFICAMOS EL HOTEL

function updateHotel(req, res) {

    upload(req, res, function (err) {

        if (err) {

            res.status(500).send({
                message: "La extensión no es correcta",
                object: null,
                response: false
            });

            return;
        }

        let isValid = validateHotelUpdate(req);

        if (isValid.valid == false) {

            res.status(422).send({
                message: isValid.message,
                object: null,
                response: false
            });

            return;

        } else {

            var idHotel = req.params.idHotel;
            var hotelInfo = {};

            hotelInfo.name = req.body.name;
            hotelInfo.stars = req.body.stars;
            hotelInfo.price = req.body.price;
            hotelInfo.amenities = [];

            var amenities = req.body.amenities.split(',');

            amenities.forEach(idAmenitie => {
                
                var amenitie = new modelAmenitie();
                amenitie._id = idAmenitie.trim();
                
                hotelInfo.amenities.push(amenitie._id);

            });
            
            if (req.file) {
                hotelInfo.image = req.file.filename;
            }

            modelHotel.findByIdAndUpdate(idHotel, hotelInfo, (error, hotelUpdated) => {

                if (error) {
                    res.status(500).send({
                        message: "Ocurrio un error en el servidor al intentar actualizar",
                        object: error,
                        response: false
                    });
                } else {

                    if (!hotelUpdated) {
                        res.status(404).send({
                            message: "Ocurrio un error al intentar actualizar",
                            object: error,
                            response: false
                        });
                    } else {

                        if (req.file) {

                            var pathToUnlink = './' + hotelUpdated.image;
                            console.log(pathToUnlink);
                            fs.exists(pathToUnlink, function (exists) {
                                if (exists) {
                                    fs.unlink(pathToUnlink);
                                }
                            });

                        }

                        res.status(200).send({
                            message: "Hotel actualizada correctamente",
                            object: hotelUpdated,
                            response: true
                        });

                    }

                }

            });
        }
    });
}


// ELIMINAMOS EL HOTEL 

function deleteHotel(req, res) {

    var idHotel = req.params.idHotel;

    modelHotel.findByIdAndRemove(idHotel, (error, hotelDeleted) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar eliminar un hotel",
                object: null,
                response: false
            });
        } else {

            if (!hotelDeleted) {
                res.status(404).send({
                    message: "Ocurrio un error al intentar eliminar un hotel",
                    object: null,
                    response: false
                });
            } else {

                var pathToUnlink = './' + hotelDeleted.image;
                console.log(pathToUnlink);
                fs.exists(pathToUnlink, function (exists) {
                    if (exists) {
                        fs.unlink(pathToUnlink);
                    }
                });

                res.status(200).send({
                    message: "Hotel eliminado correctamente",
                    object: hotelDeleted,
                    response: true
                });

            }

        }


    });

}


//LISTAMOS LOS HOTELES

function listHotels(req, res) {
    
    modelHotel.find().populate('amenities').sort('name').exec((error, hotels) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar listar los hoteles",
                object: null,
                response: false
            });
        } else {

            if (!hotels) {
                res.status(400).send({
                    message: "Ocurrio un error al intentar listar los hoteles",
                    object: null,
                    response: false
                });
            } else {
                let variable = JSON.parse(JSON.stringify(miConstData));
                hotels.forEach(hotel => {
                    variable.push(hotel);
                });
                res.status(200).send({
                    message: "Listado de hoteles",
                    object: variable,
                    response: true
                });

            }

        }

    });

}


// CONSULTAMOS UN HOTEL EN ESPECIFICO

function getHotel(req, res) {

    var idHotel = req.params.idHotel;

    modelHotel.findById(idHotel).populate('amenities').exec((error, hotel) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar listar el hotel",
                object: null,
                response: false
            });
        } else {

            if (!hotel) {
                res.status(404).send({
                    message: "Ocurrio un error al intentar listar el hotel",
                    object: null,
                    response: false
                });
            } else {

                res.status(200).send({
                    message: "Hotel encontrado",
                    object: hotel,
                    response: true
                });

            }

        }

    });

}


//GUADAMOS LA RUTA DE LA IMAGEN

function recoverImage(req, res) {

    var image = req.params.image;

    var pathImage = './uploads/hotels/' + image;

    fs.exists(pathImage, function (exists) {
        if (exists) {

            res.sendFile(path.resolve(pathImage));

        } else {
            res.status(404).send({
                message: "No se encuentra la imagen del hotel o no existe.",
                object: null,
                response: false
            });
        }
    });

}

module.exports = {

    saveHotel,
    updateHotel,
    deleteHotel,
    listHotels,
    getHotel,
    recoverImage

}