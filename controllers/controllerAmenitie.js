'use strict'

var modelAmenitie = require("./../model/modelAmenitie");
var fs = require('fs');
var path = require('path');

var multer = require('multer')



// CREAMOS DIRECTORIO UPLOA AMENITIES APRA GUARDAR LOS AMENITIES
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/amenities')
    },
    filename: function (req, file, cb) {
        var imageName = file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];
        cb(null, imageName)
    }
});


// SUMOS LA IMAGEN Y ALA VEZ LA VALIDAMOS DE ACUERDO A LA EXTENSION
var upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) { //Archivo a procesar
        let extension = (file.originalname.split('.')[file.originalname.split('.').length - 1]).toLowerCase();
        if (['png', 'jpg', 'gif', 'jpeg','svg'].indexOf(extension) === -1) {
            return callback(new Error('La extensión no cumple'));
        }
        callback(null, true);
    }
}).single('file');


//  VALIDAMS LOS CAMPOS OBLIGATORIOS DEL AMENITIE
function validateAmenitie(req) {

    let valid = true;
    let message = '';

    if (!req.file) {
        valid = false;
        message += 'La imagen de la comodidad es requerida. ';
    }

    if (req.body.name !== undefined) {

        if (req.body.name.trim() === '') {
            valid = false;
            message += 'El nombre de la comodidad es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de nombre de la comodidad es requerido. ';
    }

    return {
        valid: valid,
        message: message
    };
}

// VALIDAMOS EL AMENITIE CUANDO LO MODIFICAMOS
function validateAmenitieUpdate(req) {

    let valid = true;
    let message = '';

    if (req.body.name !== undefined) {

        if (req.body.name.trim() === '') {
            valid = false;
            message += 'El nombre de la comodidad es requerido. ';
        }

    } else {
        valid = false;
        message += 'El campo de nombre de la comodidad es requerido. ';
    }

    return {
        valid: valid,
        message: message
    };
}


// GUARDAMOS EL AMENITIE
function saveAmenitie(req, res) {

    var amenitie = new modelAmenitie();

    upload(req, res, function (err) {

        if (err) {
            res.status(500).send({
                message: "La extensión no es correcta",
                object: null,
                response: false
            });

            return;
        }

        let isValid = validateAmenitie(req);

        if (isValid.valid == false) {

            res.status(422).send({
                message: isValid.message,
                object: null,
                response: false
            });

            return;

        } else {

            amenitie.name = req.body.name;
            amenitie.image = req.file.filename;

            amenitie.save((error, newAmenitie) => {

                if (error) {

                    res.status(500).send({
                        message: "Ocurrio un error en el servidor al intentar guardar.",
                        object: null,
                        response: false
                    });

                } else {

                    if (!newAmenitie) {

                        res.status(400).send({
                            message: "Ocurrio un error al intentar guardar",
                            object: null,
                            response: false
                        });

                    } else {

                        res.status(200).send({
                            message: "Comodidad guardada correctamente",
                            object: newAmenitie,
                            response: true
                        });

                    }

                }

            });

        }
    });
}


// MODIFICAMOS EL AMENITIE DE LA CARGA DE IMAGEN
function updateAmenitie(req, res) {

    upload(req, res, function (err) {

        if (err) {

            res.status(500).send({
                message: "La extensión no es correcta",
                object: null,
                response: false
            });

            return;
        }

        let isValid = validateAmenitieUpdate(req);

        if (isValid.valid == false) {

            res.status(422).send({
                message: isValid.message,
                object: null,
                response: false
            });

            return;

        } else {

            var idAmenitie = req.params.idAmenitie;
            var amenitieInfo = {};
            amenitieInfo.name = req.body.name;

            if (req.file) {
                amenitieInfo.image = req.file.filename;
            }

            modelAmenitie.findByIdAndUpdate(idAmenitie, amenitieInfo, (error, amenitieUpdated) => {

                if (error) {
                    res.status(500).send({
                        message: "Ocurrio un error en el servidor al intentar actualizar",
                        object: null,
                        response: false
                    });
                } else {

                    if (!amenitieUpdated) {
                        res.status(404).send({
                            message: "Ocurrio un error al intentar actualizar",
                            object: null,
                            response: false
                        });
                    } else {

                        if (req.file) {

                            var pathToUnlink = './' + amenitieUpdated.image;
                            console.log(pathToUnlink);
                            fs.exists(pathToUnlink, function (exists) {
                                if (exists) {
                                    fs.unlink(pathToUnlink);
                                }
                            });

                        }

                        res.status(200).send({
                            message: "Comodidad actualizada correctamente",
                            object: amenitieUpdated,
                            response: true
                        });

                    }

                }

            });
        }
    });
}


// ELIMINAMOS EL AMENITIE

function deleteAmenitie(req, res) {

    var idAmenitie = req.params.idAmenitie;

    modelAmenitie.findByIdAndRemove(idAmenitie, (error, amenitieDeleted) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar eliminar una comodidad",
                object: null,
                response: false
            });
        } else {

            if (!amenitieDeleted) {
                res.status(404).send({
                    message: "Ocurrio un error al intentar eliminar una comodidad",
                    object: null,
                    response: false
                });
            } else {

                var pathToUnlink = './' + amenitieDeleted.image;
                console.log(pathToUnlink);
                fs.exists(pathToUnlink, function (exists) {
                    if (exists) {
                        fs.unlink(pathToUnlink);
                    }
                });

                res.status(200).send({
                    message: "Comodidad eliminado correctamente",
                    object: amenitieDeleted,
                    response: true
                });

            }

        }


    });

}


//LISTAMOS TODOS LOS AMENITIES

function listAmenities(req, res) {

    modelAmenitie.find().sort('name').exec((error, amenities) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar listar las comodidades",
                object: null,
                response: false
            });
        } else {

            if (!amenities) {
                res.status(400).send({
                    message: "Ocurrio un error al intentar listar las comodidades",
                    object: null,
                    response: false
                });
            } else {

                res.status(200).send({
                    message: "Listado de comodidades",
                    object: amenities,
                    response: true
                });

            }

        }

    });

}


// CONSULTAMOS UN AMENITIE EN ESPECIFICO
function getAmenitie(req, res) {

    var idAmenitie = req.params.idAmenitie;

    modelAmenitie.findById(idAmenitie, (error, amenitie) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar listar la comodidad",
                object: null,
                response: false
            });
        } else {

            if (!amenitie) {
                res.status(404).send({
                    message: "Ocurrio un error al intentar listar la comodidad",
                    object: null,
                    response: false
                });
            } else {

                res.status(200).send({
                    message: "Comodidad encontrada",
                    object: amenitie,
                    response: true
                });

            }

        }

    });

}

// GUARDAMOS LA RUTA DEL AMENITIE
function recoverImage(req, res) {

    var image = req.params.image;

    var pathImage = './uploads/amenities/' + image;

    fs.exists(pathImage, function (exists) {
        if (exists) {

            res.sendFile(path.resolve(pathImage));

        } else {
            res.status(404).send({
                message: "No se encuentra la imagen de la comodidad o no existe.",
                object: null,
                response: false
            });
        }
    });

}

module.exports = {

    saveAmenitie,
    updateAmenitie,
    deleteAmenitie,
    listAmenities,
    getAmenitie,
    recoverImage

}